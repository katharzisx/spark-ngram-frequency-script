//Fetch data from Cassandra Eoi Table
//App_ID, user_id order by crawl_ts asc order

//Get cassandra data into a dataframe
//where app_title = "" and global_element_id IS NOT NULL
//order by crawl_timestamp ASCENDING
//import
import org.apache.spark.sql.cassandra.CassandraSQLContext
or
val mycsc = new org.apache.spark.sql.cassandra.CassandraSQLContext(sc); 

scala> val raw_eoi_input = mycsc.sql("SELECT * FROM jefftestkeyspace.eoi_raw_1 WHERE app_title = 'calc' AND global_element_id IS NOT NULL  ORDER BY crawl_timestamp ASC")

//preprocess dataframe
//filter based on control types
//remove consecutive duplicate eoi(to avoid mistake clicks by users)
//take only the first part of the id

val sequenceDF = raw_eoi_input.select("global_element_id")

sequenceDF.show(false)
+------------------------------------+
|global_element_id                   |
+------------------------------------+
|96aae820-4ad2-11e7-b4e7-27e76fe0cfd9|
|96a9d6b2-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aa9a01-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aae821-4ad2-11e7-b4e7-27e76fe0cfd9|
|9855d901-4ad2-11e7-b4e7-27e76fe0cfd9|
|98059620-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aa24d4-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aae821-4ad2-11e7-b4e7-27e76fe0cfd9|
|9855d901-4ad2-11e7-b4e7-27e76fe0cfd9|
|98059620-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aa72fe-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aae821-4ad2-11e7-b4e7-27e76fe0cfd9|
|9855d901-4ad2-11e7-b4e7-27e76fe0cfd9|
|98059620-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aa72fe-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aae820-4ad2-11e7-b4e7-27e76fe0cfd9|
|96a9d6b2-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aa9a01-4ad2-11e7-b4e7-27e76fe0cfd9|
|97114b12-4ad2-11e7-b4e7-27e76fe0cfd9|
|96aae820-4ad2-11e7-b4e7-27e76fe0cfd9|
+------------------------------------+
only showing top 20 rows


//get only global id and split its contents
//scala> val rddSequence = sequenceDF.map( x=> { Row(x.getAs[String]("global_element_id"))})
import org.apache.spark.sql.Row
//scala> val rddSequence = sequenceDF.map( x=> { val y = x.getAs[String]("global_element_id"); val ar_y = y.split("-");Row(ar_y(0))})
scala> 
//val rddSequence = sequenceDF.map( x=> { val y = x.getAs[String]("global_element_id"); val ar_y = y.split("-");Seq(ar_y(0))})
val rddSequence = sequenceDF.map( x=> { val y = x.getAs[String]("global_element_id"); val ar_y = y.split("-");Row(Array(ar_y(0)))})
val rddSequence = sequenceDF.map( x=> { val y = x.getAs[String]("global_element_id"); val ar_y = y.split("-");ar_y(0)})
rddSequence.take(10).foreach(println)

rddSequence.take(10).foreach(println)
96aae820
96a9d6b2
96aa9a01
96aae821
9855d901
98059620
96aa24d4
96aae821
9855d901
98059620

val strData = rddSequence.toArray.mkString(",")
val arrStrData = rddSequence.toArray
val mainArr = Array(arrStrData)

val rddSeqMain = sc.parallelize(mainArr)
rddSeqMain.take(10).foreach(println)
[Ljava.lang.String;@68440c95

//we need to have them as a row object so that we can convert it to df
//createDataFrame expects rdd containing row type
val rddSequence = rddSeqMain.map( x=> {Row(x)})

rddSequence.take(10).foreach(println)

val my_schema = StructType(Array(StructField("item", ArrayType(StringType,true), true)))
val item_df = mycsc.createDataFrame(rddSequence, my_schema)
item_df.show(false)

|[96aae820, 96a9d6b2, 96aa9a01, 96aae821, 9855d901, 98059620, 96aa24d4, 96aae821, 9855d901, 98059620, 96aa72fe, 96aae821, 9855d901, 98059620, 96aa72fe, 96aae820, 96a9d6b2, 96aa9a01, 97114b12, 96aae820, 96a9d6b2, 96aa9a01, 96aae820, 96a9d6b2, 96aa9a01, 96aae821, 9855d901, 98059620, 96aa72fe]|

//create ngrams

val seq_ngram = new NGram().setN(3).setInputCol("item").setOutputCol("ngrams")
val ngramDataFrame = seq_ngram.transform(item_df)
ngramDataFrame.select("ngrams").show(false)

|[96aae820 96a9d6b2 96aa9a01, 96a9d6b2 96aa9a01 96aae821, 96aa9a01 96aae821 9855d901, 96aae821 9855d901 98059620, 9855d901 98059620 96aa24d4, 98059620 96aa24d4 96aae821, 96aa24d4 96aae821 9855d901, 96aae821 9855d901 98059620, 9855d901 98059620 96aa72fe, 98059620 96aa72fe 96aae821, 96aa72fe 96aae821 9855d901, 96aae821 9855d901 98059620, 9855d901 98059620 96aa72fe, 98059620 96aa72fe 96aae820, 96aa72fe 96aae820 96a9d6b2, 96aae820 96a9d6b2 96aa9a01, 96a9d6b2 96aa9a01 97114b12, 96aa9a01 97114b12 96aae820, 97114b12 96aae820 96a9d6b2, 96aae820 96a9d6b2 96aa9a01, 96a9d6b2 96aa9a01 96aae820, 96aa9a01 96aae820 96a9d6b2, 96aae820 96a9d6b2 96aa9a01, 96a9d6b2 96aa9a01 96aae821, 96aa9a01 96aae821 9855d901, 96aae821 9855d901 98059620, 9855d901 98059620 96aa72fe]|

//Get ngrams frequencies

val ngramSeq = ngramDataFrame.select("ngrams")
ngramSeq.show(false)
[CALACULATE COUNT OF EACH NGRAM]

//remove array brackets
//each line will have ngram sequences separated by ,
//val divNGram = ngramSeq.map( x=> {   var arr_y = x.getAs[Seq[String]]("ngrams");val y = arr_y(0); y})
val divNGram = ngramSeq.rdd.map( x=> {   var arr_y = x.getAs[Seq[String]]("ngrams");val y = arr_y.mkString(","); y})
divNGram.take(10).foreach(println)

96aae820 96a9d6b2 96aa9a01,
96a9d6b2 96aa9a01 96aae821,
96aa9a01 96aae821 9855d901,
96aae821 9855d901 98059620,9855d901 98059620 96aa24d4,98059620 96aa24d4 96aae821,96aa24d4 96aae821 9855d901,96aae821 9855d901 98059620,9855d901 98059620 96aa72fe,98059620 96aa72fe 96aae821,96aa72fe 96aae821 9855d901,96aae821 9855d901 98059620,9855d901 98059620 96aa72fe,98059620 96aa72fe 96aae820,96aa72fe 96aae820 96a9d6b2,96aae820 96a9d6b2 96aa9a01,96a9d6b2 96aa9a01 97114b12,96aa9a01 97114b12 96aae820,97114b12 96aae820 96a9d6b2,96aae820 96a9d6b2 96aa9a01,96a9d6b2 96aa9a01 96aae820,96aa9a01 96aae820 96a9d6b2,96aae820 96a9d6b2 96aa9a01,96a9d6b2 96aa9a01 96aae821,96aa9a01 96aae821 9855d901,96aae821 9855d901 98059620,9855d901 98059620 96aa72fe


//flatten the string with , so that each ngram has its own row
val divNGramFlat = divNGram.flatMap(x=>x.split(","))
divNGramFlat.take(10).foreach(println)

96aae820 96a9d6b2 96aa9a01
96a9d6b2 96aa9a01 96aae821
96aa9a01 96aae821 9855d901
96aae821 9855d901 98059620
9855d901 98059620 96aa24d4
98059620 96aa24d4 96aae821
96aa24d4 96aae821 9855d901
96aae821 9855d901 98059620
9855d901 98059620 96aa72fe
98059620 96aa72fe 96aae821

//Map reduce
val ngFreq = divNGramFlat.map(x => (x, 1)).reduceByKey{case(a,b)=>a+b}
ngFreq.collect().foreach(println)

(97114b12 96aae820 96a9d6b2,1)
(96aa9a01 96aae821 9855d901,2)
(96aae821 9855d901 98059620,4)
(96a9d6b2 96aa9a01 96aae820,1)
(9855d901 98059620 96aa24d4,1)
(9855d901 98059620 96aa72fe,3)
(96aa72fe 96aae820 96a9d6b2,1)
(96a9d6b2 96aa9a01 96aae821,2)
(98059620 96aa72fe 96aae820,1)
(96a9d6b2 96aa9a01 97114b12,1)
(96aa9a01 97114b12 96aae820,1)
(96aa24d4 96aae821 9855d901,1)
(98059620 96aa72fe 96aae821,1)
(96aa9a01 96aae820 96a9d6b2,1)
(96aa72fe 96aae821 9855d901,1)
(98059620 96aa24d4 96aae821,1)
(96aae820 96a9d6b2 96aa9a01,4)

//sort by value (count)

val orderedSeqCount = ngFreq.map{case (seq, count) => (count, seq)}
orderedSeqCount.sortByKey(true)
orderedSeqCount.top(10)

Array((4,96aae821 9855d901 98059620), 
	(4,96aae820 96a9d6b2 96aa9a01), 
	(3,9855d901 98059620 96aa72fe), 
	(2,96aa9a01 96aae821 9855d901), 
	(2,96a9d6b2 96aa9a01 96aae821), 
	(1,9855d901 98059620 96aa24d4), 
	(1,98059620 96aa72fe 96aae821), (1,98059620 96aa72fe 96aae820), (1,98059620 96aa24d4 96aae821), (1,97114b12 96aae820 96a9d6b2))

***********************
//convert them to strings then back to one row containing all as one sequence
//val strData = rddSequence.reduce((s1, s2) => s1 + " " + s2) //do not use changes the order of elements
//array of strings 
//val arrStrData = strData.split(" ")
//val arrStrData = Array(strData)
//val mainArr = Array(arrStrData)
val rddSeqMain = sc.parallelize(mainArr)
rddSeqMain.take(10).foreach(println)
//we need to have them as a row object so that we can convert it to df
//createDataFrame expects rdd containing row type
val rddSequence = rddSeqMain.map( x=> {Row(x)})

rddSequence.take(10).foreach(println)


//convert back to dataframe
//rddSequence.toDF()
import org.apache.spark.sql.types._
scala> 
// val my_schema = StructType(List(StructField("item",StringType, true)))
// val my_schema = StructType(Array(StructType("item", Array, true)))
// val my_schema = StructType(StructType(Array(StructField("item", StringType, true))))

// val my_schema = StructType(Array(StructField("item", ArrayType(StringType,true), true)))

// val my_schema = StructType(List(Array(StructField("item", StringType, true))))



// val my_schema = StructType(StructType(Array(StructField("item", StringType, true))))

val my_schema = StructType(Array(StructField("item", ArrayType(StringType,true), true)))
val item_df = mycsc.createDataFrame(rddSequence, my_schema)
item_df.show(false)

|[96aae820, 96a9d6b2, 96aa9a01, 96aae821, 9855d901, 98059620, 96aa24d4, 96aae821, 9855d901, 98059620, 96aa72fe, 96aae821, 9855d901, 98059620, 96aa72fe, 96aae820, 96a9d6b2, 96aa9a01, 97114b12, 96aae820, 96a9d6b2, 96aa9a01, 96aae820, 96a9d6b2, 96aa9a01, 96aae821, 9855d901, 98059620, 96aa72fe]|


//create ngrams

val seq_ngram = new NGram().setN(3).setInputCol("item").setOutputCol("ngrams")
val ngramDataFrame = seq_ngram.transform(item_df)
ngramDataFrame.select("ngrams").show(false)

//Get ngrams frequencies

val ngramSeq = ngramDataFrame.select("ngrams")
//there will be only 1 element i.e. one row will be one complete transaction set
// val divNGram = ngramSeq.map(x=>{
// 		var arr_y = x.getAs[Array[String]]("ngrams");
// 		val y = arr_y.mkString(",");
// 		if (y.contains(",")){
// 			val seqArr = y.split(",");
// 			val arLen = seqArr.length;
// 			var resultstr = "";
// 			for (i <- 0 until arLen-1) yield {
// 				val resultstr_1 = seqArr(i);
// 				if(resultstr.length>0){
// 				resultstr = resultstr +"@"+ resultstr_1;}
// 					else
// 				{resultstr = resultstr_1;}
// 			}
// 			resultstr;
// 		}
// 	})
//remove wrapped array
//val rddNgram = ngramSeq.rdd.map( x=> { val insidearr = x.getAs[Seq[String]]("text"); val strdata = insidearr.mkString(","); strdata  })

[CALACULATE COUNT OF EACH NGRAM]

//remove array brackets
//each line will have ngram sequences separated by ,
val divNGram = ngramSeq.rdd.map( x=> {   var arr_y = x.getAs[Seq[String]]("ngrams");val y = arr_y.mkString(","); y})
divNGram.take(10).foreach(println)
//flatten the string with , so that each ngram has its own row
val divNGramFlat = divNGram.flatMap(x=>x.split(","))
divNGramFlat.take(10).foreach(println)

//Map reduce
val ngFreq = divNGramFlat.map(x => (x, 1)).reduceByKey{case(a,b)=>a+b}
ngFreq.collect().foreach(println)


// val divNGram = ngramSeq.map(x=>{
// 	var arr_y = x.getAs[Seq[String]]("ngrams");
// 	val y = arr_y.mkString(",");
// 	if (y.contains(","))
// 	{val seqArr = y.split(",");
// 	val arLen = seqArr.length;
// 	var resultstr = "";
// 	for (i <- 0 until arLen-1) yield 
// 	{val resultstr_1 = seqArr(i);
// 		if(resultstr.length>0)
// 		{resultstr = resultstr +"@"+ resultstr_1;}
// 		else{resultstr = resultstr_1;}
// 	}
// 	resultstr}})
//val divNGram = ngramSeq.map(x=>{var arr_y = x.getAs[Seq[String]]("ngrams");val y = arr_y.mkString(",");if (y.contains(",")){val seqArr = y.split(",");val arLen = seqArr.length;var resultstr = "";for (i <- 0 until arLen-1) yield {resultstr = seqArr(i);resultstr;}}})


//dummy

// if (x.contains(",")){
// 	val strarr = x.split(",");
// 	val arLen = strarr.length; 
// 	var resultstr = "";
// 	for (i <- 0 until arLen-1) yield 
// 		{	val resultstr_1 = strarr(i) +"$"+ strarr(i+1);
// 			if(resultstr.length>0){
// 				resultstr = resultstr +"@"+ resultstr_1;}
// 			else
// 				{resultstr = resultstr_1;}
// 		};
// 	resultstr
// }

//
// var seqData = ""
// rddSequence.foreach({x=> seqData = x+seqData})

//val string_to_array_udf = udf((s:String) => Array(s))

val strData = rddSequence.reduce((s1, s2) => s1 + " " + s2)

val arrStrData = strData.split(" ")
//val arrStrData = Array(strData)
val mainArr = Array(arrStrData)
val rddSeqMain = sc.parallelize(mainArr)
rddSeqMain.take(10).foreach(println)
val rddSequence = rddSeqMain.map( x=> {Row(x)})

rddSequence.take(10).foreach(println)

//

import org.apache.spark.ml.feature.NGram
val wordDataFrame = spark.createDataFrame(Seq(
  (0, Array("Hi", "I", "heard", "about", "Spark")),
  (1, Array("I", "wish", "Java", "could", "use", "case", "classes")),
  (2, Array("Logistic", "regression", "models", "are", "neat"))
)).toDF("id", "words")

val ngram = new NGram().setN(2).setInputCol("words").setOutputCol("ngrams")

val ngramDataFrame = ngram.transform(wordDataFrame)
ngramDataFrame.select("ngrams").show(false)

//dataframe count frequencies