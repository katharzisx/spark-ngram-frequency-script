import org.apache.spark.sql.cassandra.CassandraSQLContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types._;
import org.apache.spark.ml.feature.NGram;
import com.datastax.driver.core.utils.UUIDs;

val mycsc = new org.apache.spark.sql.cassandra.CassandraSQLContext(sc); 

//get all app titles
var queryAppTitles = "SELECT DISTINCT app_title FROM jefftestkeyspace.eoi_raw_1";
val dfAppTitles = mycsc.sql(queryAppTitles);
var ar_app_list = Array("notepad", "calc");
val arAppTitles = dfAppTitles.select("app_title").map(r=>r(0).asInstanceOf[String]).collect();

for(app <- arAppTitles){
var app_name = app;
var queryEOI  = "SELECT * FROM jefftestkeyspace.eoi_raw_1 WHERE app_title = '"+app_name+"' AND global_element_id IS NOT NULL  ORDER BY crawl_timestamp ASC";
//var queryEOI  = "SELECT * FROM jefftestkeyspace.eoi_raw_1 WHERE app_title = 'notepad' AND global_element_id IS NOT NULL  ORDER BY crawl_timestamp ASC";
//println(query);

val raw_eoi_input = mycsc.sql(queryEOI);

val eoi_crawl_ts_data = raw_eoi_input.select("app_title","global_element_id", "crawl_timestamp");
//PUSH DATA TO TABLE containing mapping between uuid of seq and thier iteractionTimeStamp
//eoi_crawl_ts_data.show();
//CREATE A REPLICA OF EOI TABLE with only few imp cols(interaction timestamp)
val dict_eoiID_crawlTS = eoi_crawl_ts_data.map( x=> { val p = x.getAs[String]("app_title");val q = x.getAs[String]("global_element_id");val r = x.getAs[String]("crawl_timestamp"); (UUIDs.timeBased(), p, q, r)});
//dict_eoiID_crawlTS.take(10).foreach(println);
 dict_eoiID_crawlTS.saveToCassandra("jefftestkeyspace", "eoi_interaction_ts_1", SomeColumns("entry_id", "app_id", "eoi_id", "interaction_ts"));

//FETCH ONLY THE GLOBAL IDs
val sequenceDF = raw_eoi_input.select("global_element_id");
//sequenceDF.show(false);
//val rddSequence = sequenceDF.map( x=> { val y = x.getAs[String]("global_element_id"); val ar_y = y.split("-");ar_y(0)});
val rddSequence = sequenceDF.map( x=> { val y = x.getAs[String]("global_element_id"); y});

val strData = rddSequence.toArray.mkString(",");
//NEED TO COLLAPSE CONSECUTIVE DUPLICATES INTO ONE ITEM
var newSeqStr = "";
var arSeqString = strData.split(",");
//Array(f8bf7132, f8bf4a21, f8bf7133, f8cf28a5, f8bf7133, f8cf28a5, f8bf7133, f8cf28a5, f8bf4a21, f8bf7134, f8e45e53, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf4a21, f8bf7132, f8bf4a21, f8bf7134, f8e45e53, f8bf4a21, f8bf7134, f8bf4a21, f8bf4a21, f9a69ce1, f8bf7134, f8e45e52, f8bf7133, f8cf28a5, f8bf7133, f8cf28a5, f8bf7135, f97357e2, f8bf4a21, f8bf7135, f97357e2, f8bf4a21, f8bf7135, f97357e2, f8bf4a21, f8bf7136, f9d3a053, f8bf7133, f8cf28a5)
if(arSeqString.length>1){for( i <- 0 until arSeqString.length){var currentStr = arSeqString(i);var previousStr = "";if(!(i==0)){previousStr = arSeqString(i-1);if(!currentStr.equalsIgnoreCase(previousStr)){newSeqStr += ',' +currentStr;}}else{newSeqStr += currentStr;}}}else{newSeqStr = strData;}
//array containing 1 element : Array of Strings
//val arrStrData = rddSequence.toArray;
//val mainArr = Array(arrStrData);
val arrStrData = newSeqStr.split(",");
val mainArr = Array(arrStrData);
val rddSeqMain = sc.parallelize(mainArr);

//we need to have them as a row object so that we can convert it to df
//createDataFrame expects rdd containing row type
val rddSequence_new = rddSeqMain.map( x=> {Row(x)});

val my_schema = StructType(Array(StructField("item", ArrayType(StringType,true), true)));
val item_df = mycsc.createDataFrame(rddSequence_new, my_schema);

//NGRAMS
//loop and create 2, 3, 4 grams and save result into cassandra
// ngram range
val ngListRange = Array(2, 3, 4, 5);
//for (i <- 1 to 3) println(i)
for(e <- ngListRange){
	var n = e;

	val seq_ngram = new NGram().setN(n).setInputCol("item").setOutputCol("ngrams");
val ngramDataFrame = seq_ngram.transform(item_df);
//ngramDataFrame.select("ngrams").show(false);

val ngramSeq = ngramDataFrame.select("ngrams");
//ngramSeq.show(false);
//|[0d2089ed 0d1391ac 0d1391ab, 0d1391ac 0d1391ab 0d1391b1, 0d1391ab 0d1391b1 0d1391b2, 0d1391b1 0d1391b2 0d1391b7, 0d1391b2 0d1391b7 0d1391b6, 0d1391b7 0d1391b6 0d1391a3, 0d1391b6 0d1391a3 0d2089eb, 0d1391a3 0d2089eb 0d2089ef]|

//remove array brackets
//each line will have ngram sequences separated by ,
//val divNGram = ngramSeq.map( x=> {   var arr_y = x.getAs[Seq[String]]("ngrams");val y = arr_y(0); y})
val divNGram = ngramSeq.rdd.map( x=> {   var arr_y = x.getAs[Seq[String]]("ngrams");val y = arr_y.mkString(","); y});
//divNGram.take(10).foreach(println);
//0d2089ed 0d1391ac 0d1391ab,0d1391ac 0d1391ab 0d1391b1,0d1391ab 0d1391b1 0d1391b2,0d1391b1 0d1391b2 0d1391b7,0d1391b2 0d1391b7 0d1391b6,0d1391b7 0d1391b6 0d1391a3,0d1391b6 0d1391a3 0d2089eb,0d1391a3 0d2089eb 0d2089ef

//flatten the string with , so that each ngram has its own row
val divNGramFlat = divNGram.flatMap(x=>x.split(","));
//divNGramFlat.take(10).foreach(println)
/*
0d2089ed 0d1391ac 0d1391ab
0d1391ac 0d1391ab 0d1391b1
0d1391ab 0d1391b1 0d1391b2
0d1391b1 0d1391b2 0d1391b7
0d1391b2 0d1391b7 0d1391b6
0d1391b7 0d1391b6 0d1391a3
0d1391b6 0d1391a3 0d2089eb
0d1391a3 0d2089eb 0d2089ef
*/
//Map reduce
val ngFreq = divNGramFlat.map(x => (x, 1)).reduceByKey{case(a,b)=>a+b};
//ngFreq.collect().foreach(println);
/*
(0d1391b1 0d1391b2 0d1391b7,1)
(0d1391b7 0d1391b6 0d1391a3,1)
(0d2089ed 0d1391ac 0d1391ab,1)
(0d1391ac 0d1391ab 0d1391b1,1)
(0d1391a3 0d2089eb 0d2089ef,1)
(0d1391b6 0d1391a3 0d2089eb,1)
(0d1391b2 0d1391b7 0d1391b6,1)
(0d1391ab 0d1391b1 0d1391b2,1)
*/
//Set a threshold, take only those freq_seq whose freq is above a certain threshold
var threshold = 1;
val filteredfreqSeq = ngFreq.filter( x=> x._2.toInt>threshold);
//insert to cassandra table app_freq_seq table
var timeNowUuid = UUIDs.timeBased();
var app_id = app_name;
val updatedRDD = filteredfreqSeq.map(r => Tuple4(UUIDs.timeBased(), app_id, r._1, r._2.toInt ));

updatedRDD.saveToCassandra("jefftestkeyspace", "app_freq_seq_1", SomeColumns("entry_id", "app_id", "freq_seq", "freq"));

}
//end n gram for loop
}
//end app for loop

/*
//TO SEE THE TOP 10
val orderedSeqCount = ngFreq.map{case (seq, count) => (count, seq)};
orderedSeqCount.sortByKey(true);
orderedSeqCount.top(10);
*/

System.exit(0);

